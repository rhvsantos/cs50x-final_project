# OneTwoThree Lectures
#### Video Demo:  https://youtu.be/sF-SHOq6Pyc

OneTwoThree Lectures was my final project for CS50 Harvard course. This course had about 10 modules separeted by week, and each week We've seen 
a subject, such as: SQL, Pyton, Flask, C, Javascript, HTML and CSS and some logical fundamentals. And after these 10 modules, we had to submit a final
project using some of these technologies we've learned. So, I did this app by myself :)


#### Description:
So, what I've done here was a web application that allows people to organize their lectures. For example, if you are a professor, and want to do a lecture to a coleage, you will need a place where participants can subscribe to your lecture. And you need to have a control about how many participants are enroll with your lecture, the name of each participant but the most important thing, you need to give a certificate for each participant. If I would resume what this app doest, it will be that.

To do this project, I've used Python using Flask framework. For the frontend, I've just used HTML, CSS and a little bit of Javascripts. I've worked
with Jinja too, to do the templates.

### With more details:

When you access the application, there will be 2 sections at the index page, one for the Participant and the another One for admin users:

- **Participant Section:** Here the participants can see all the available lectures, the lectures that they are enroll with and get certificates.

  **How do I distinguish the participants ?**

  To be a participant, first you need to submit a form with some informations, such as, First name, last Name, Birthday date, email, etc. Then, this person will receive
  an email containing the registration code, that will be needed to access the participant area.
  In the flask app, I've imported the sessions library, so I can save the user's session.

- **Admin Section:** It is a section where de admin users can create/edit/delete lectures, insert new professors, see all participants of each lecture, manage admin users, etc.

For me, the most interesting point of this project was to automatically generete the certificate for the participants. My first idea, was to upload a Certificate Model (in PDF) when the lecture is created, and after, with some PDF library, i would work with REGEX or some other thing to make the certificate for each participant.

But later, I've found the CraftMyPDF, that has an REST API, so I sign up creating an account there, and using the free plan, I have 50 credits to use per month (each credit correspond for each API Call), and I've implemented a function the makes API Calls when the participant click on "Get Certificate", so, the person will be redirected to the certificate where they can download it, and in this Certificate are, The participant's name, Professor's name, lecture's name...