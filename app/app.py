from helpers import login_required, mixedcase_word, allowed_file, reg_code_required, make_certificate
from flask import Flask, flash, redirect, render_template, request, session, g
from werkzeug.security import check_password_hash, generate_password_hash
from email.mime.multipart import MIMEMultipart
from flask_session import Session
from email.mime.text import MIMEText
import smtplib, datetime, os, re
import logging
import sqlite3
import json
import sys

# Define the app
app = Flask(__name__)

# SECRET_KEY (CHANGE THIS)
app.secret_key = 'f366fb97413b13af12d4967ae2504fa0da443325c1c504562728314307236ef8'

# DB file
DB = "app.db"


def make_dicts(cursor, row):
	return dict((cursor.description[idx][0],value) for idx, value in enumerate(row))

def get_db():
	db = getattr(g, '_database', None)
	if db is None:
		db = g._database = sqlite3.connect(DB)
		db.row_factory = make_dicts
	return db

def sql_query(query, args=()):
	cur = get_db().execute(query, args)
	rv = cur.fetchall()
	cur.close()
	return rv


# Configure logging parameters
logger = logging.getLogger('werkzeug')
handler = logging.FileHandler('access.log')
logger.addHandler(handler)

def init_db():
    with app.app_context():
    	db = get_db()
    	with app.open_resource('schema.sql', mode='r') as f:
    	    db.cursor().executescript(f.read())
    db.commit()

# Importing sql schema if the database doesn't exist
try:
    check_db = open(DB, "r")
    check_db.close()
except IOError:
    init_db()

# Configure folder to upload the PDF files (is not being used)
path = os.getcwd()
UPLOAD_FOLDER = os.path.join(path, 'uploads')

if not os.path.isdir(UPLOAD_FOLDER):
	os.mkdir(UPLOAD_FOLDER)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# Ensure templates are auto-reloaded
app.config["TEMPLATES_AUTO_RELOAD"] = True

# Configure smtp
# Get parameters from os variables
SMTP_SERVER = os.environ.get("SMTP_SERVER")
SMTP_SERVER_PORT = os.environ.get("SMTP_SERVER_PORT")
SMTP_ADDR = os.environ.get("SMTP_ADDR")
SMTP_PASSWD = os.environ.get("SMTP_PASSWD")
server = smtplib.SMTP(f"{SMTP_SERVER}: {SMTP_SERVER_PORT}")
SMTP_ADDRESS = SMTP_ADDR

# Define session's settings
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

@app.after_request
def after_request(response):
	"""Ensure responses aren't cached"""
	response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
	response.headers["Expires"] = 0
	response.headers["Pragma"] = "no-cache"
	return response

@app.route("/")
def index():
	return render_template("index.html")

@app.route("/about")
def about():
	return render_template("about.html")

@app.route("/contact", methods=["GET", "POST"])
def contact():

	# POST
	if request.method == "POST":

		# Ensure all fiels are filled
		f_name = request.form.get("f_name")
		l_name = request.form.get("l_name")
		email = request.form.get("email")
		message = request.form.get("message")

		if not f_name or not l_name or not email or not message:
			flash("Please, fill in all fields.")
			return redirect("/contact")

		# Send email
		msg = MIMEMultipart()

		# Configure the message
		msg['From'] = SMTP_ADDRESS
		msg['To'] = SMTP_ADDRESS
		msg['Subject'] = "New message from " + f_name + " " + l_name + " - " + email

		msg.attach(MIMEText(message, 'plain'))

		# SMTP Log in
		try:
			server.starttls()
			server.login(msg['From'], SMTP_PASSWD)
		except:
			flash("SMTP Server is unavailable, please try later.")
			return redirect("/contact")

		# Send the email
		server.sendmail(msg['From'], msg['To'], msg.as_string())

		# Close server connection
		server.quit()

		# Log
		print("successfully received email from " + email)

		# Redirect
		flash("Thanks! We received your message.")
		return redirect("/")

	# GET
	return render_template("contact.html")

@app.route("/login", methods=["GET", "POST"])
def login():
	"""Log user in"""

	# Clear session
	session.clear()

	# POST
	if request.method == "POST":

		# Ensure username was submitted
		if not request.form.get("login"):
			flash('Login is empty.')
			return redirect("/login/error")

		# Ensure password was submitted
		elif not request.form.get("password"):
			flash('Password is empty.')
			return redirect("/login/error")

		# Query database for username
		user_query = sql_query("SELECT hash_password, status, id FROM users WHERE username = ?", [request.form.get("login")])

		# Ensure username exists and password is correct
		if len(user_query) != 1 or not check_password_hash(user_query[0]['hash_password'], request.form.get("password")):
			flash('Wrong Login and/or password.')
			return redirect("/login/error")

		if user_query[0]["status"] == 1:
			flash("Your user is disabled.")
			return redirect("/login/error")

		# Remember which user has logged in
		session["user_id"] = user_query[0]["id"]

		# Invalidate validation_code's field
		sql_query("UPDATE users SET validation_code = ? WHERE username = ?", ["null", request.form.get("login")])

		# Register login
		date_now = datetime.datetime.now()
		date = f'{date_now.year}-{date_now.month}-{date_now.day} {date_now.hour}:{date_now.minute}:{date_now.second}'
		sql_query("UPDATE users SET last_login = ? WHERE username = ?", [date, request.form.get("login")])

		# Changes
		get_db().commit()

		flash('Logged In.')
		return redirect("/admin")

	# GET
	return render_template("login.html")

@app.route("/logout")
def logout():
	# Forget any user_id
	session.pop('user_id', default=None)

	flash('Logged out!')
	return redirect("/")

@app.route("/login/error")
def error():
	return render_template("login.html")

@app.route("/admin")
@login_required
def admin():
	return render_template("admin.html")


@app.route("/manage_users")
@login_required
def manage_users():

	# Query for users in user's table
	manage_users = sql_query("SELECT id, username, email, last_login, last_pw_change, status FROM users")

	# Render the page
	return render_template("manage_users.html", users=manage_users)

@app.route("/new_user", methods=["GET", "POST"])
@login_required
def add_user():

	# Query for users in user's table
	query_users = sql_query("SELECT id, username, email, status FROM users")

	# POST
	if request.method == "POST":

		# Get username, password and status
		login = request.form.get("login")
		passwd = request.form.get("password")
		email = request.form.get("email")
		disabled = request.form.get("disabled")

		# Ensure username and/or password isn't empty
		if not login:
			flash("Username is empty.")
			return redirect("/new_user/error")
		elif not passwd:
			flash("Password is empty.")
			return redirect("/new_user/error")
		elif not email:
			flash("Email is empty.")
			return redirect("/new_user/error")

		# Ensure the login does not exist in database
		matched_users = sql_query("SELECT username FROM users WHERE username = ? OR email = ?", [login, email])

		if len(matched_users) > 0:
			
			flash("This login/email already exist, choose another one.")
			return redirect("/new_user/error")

		# Hash the password
		hashpw = generate_password_hash(passwd)

		# Get timedate
		date_now = datetime.datetime.now()
		date = f'{date_now.year}-{date_now.month}-{date_now.day} {date_now.hour}:{date_now.minute}:{date_now.second}'

		# Insert the new user into the user's table
		if disabled:
			sql_query("INSERT INTO users (username, email, hash_password, status, created_at) VALUES(?, ?, ?, ?, ?)", [login, email, hashpw, 1, date])
		else:
			sql_query("INSERT INTO users (username, email, hash_password, status, created_at) VALUES(?, ?, ?, ?, ?)", [login, email, hashpw, 0, date])

		# Commiting changes
		get_db().commit()

		# Return to the main page
		flash('The user has been created.')
		return redirect("/manage_users")


	# GET
	return render_template("add_user.html")

@app.route("/new_user/error", methods=["GET"])
@login_required
def new_user_error():
	return render_template("add_user.html")

@app.route("/edit_user", methods=["GET", "POST"])
@login_required
def edit_user():

	# Query for users in user's table
	query_users = sql_query("SELECT id, username, email, status FROM users")

	# POST
	if request.method == "POST":
		login = request.form.get("login")
		passwd = request.form.get("password")
		disabled = request.form.get("disabled")
		email = request.form.get("email")

		# Update user's status
		if disabled:
			sql_query("UPDATE users SET status = ? WHERE id = ?", [1, login])
		else:
			sql_query("UPDATE users SET status = ? WHERE id = ?", [0, login])

		# Update user's password
		if passwd:
			hashpw = generate_password_hash(passwd)
			sql_query("UPDATE users SET hash_password = ? WHERE id = ?", [hashpw, login])

			# Save password change datetime
			date_now = datetime.datetime.now()
			date = f'{date_now.year}-{date_now.month}-{date_now.day} {date_now.hour}:{date_now.minute}:{date_now.second}'
			sql_query("UPDATE users SET last_pw_change = ? WHERE id = ?", [date, login])

		if email:
			# Ensure the email is not beeing used for another user
			query = sql_query("SELECT * FROM users WHERE email = ? AND NOT id = ?", [email, login])
			
			if len(query) == 0:
				sql_query("UPDATE users SET email = ? WHERE id = ?", [email, login])
			else:
				get_db().close()
				flash("This email already exist in database.")
				return redirect("/edit_user")
		
		# Commiting changes
		get_db().commit()

		# Redirect the user
		flash("The changes have been saved.")
		return redirect("/manage_users")

	# GET

	# If admin wants to edit an user into list users page
	try:
		login_id = int(request.args.get("id"))

		if login_id:
			return render_template("edit_user.html", users=query_users, login_id=login_id)
	except:
		return render_template("edit_user.html", users=query_users)

#@app.route("/error_edit")
#@login_required
#def error_edit():
#
#	query_users = sql_query("SELECT id, username, email, status FROM users")
#	return render_template("edit_user.html", users=query_users)

@app.route("/delete_user", methods=["POST", "GET"])
@login_required
def delete_user():

	# Query for users in user's table
	query_users = sql_query("SELECT id, username, status FROM users")

	# POST
	if request.method == "POST":

		# Get login to delete
		login = request.form.get("login")

		if not login:
			return redirect("/delete_user")

		# Delete user from database
		sql_query("DELETE FROM users WHERE id = ?", [login])

		# Commiting changes
		get_db().commit()

		# Redirects the user
		flash("The user has been deleted.")
		return redirect("/manage_users")

	# GET

	# If admin wants to edit an user into list users page
	try:
		login_id = int(request.args.get("id"))

		if login_id:
			return render_template("delete_user.html", users=query_users, login_id=login_id)
	except:
		return render_template("delete_user.html", users=query_users)

@app.route("/forgot", methods=["GET", "POST"])
def forgot_pw():

	# POST
	if request.method == "POST":

		# Get email that was submitted
		get_email = request.form.get("email")

		# Check wether the email is valid
		query = sql_query("SELECT email FROM users WHERE email = ?", [get_email])

		if len(query) == 1:

			# Generate random code to send (Got from https://kudadam.com/blog/random-words-python)
			random_word = mixedcase_word()

			# Send random code to the user's email
			msg = MIMEMultipart()

			# Configure the message
			message = "Hello, your code is: " + random_word
			msg['From'] = SMTP_ADDRESS
			msg['To'] = get_email
			msg['Subject'] = "[OneTwoThreeLectures] - Here is your code"

			msg.attach(MIMEText(message, 'plain'))
			
			# SMTP Log in
			try:
				server.starttls()
				server.login(msg['From'], SMTP_PASSWD)
			except:
				flash("SMTP Server is unavailable, please try later.")
				return redirect("/forgot")

			# Send the email
			server.sendmail(msg['From'], msg['To'], msg.as_string())

			# Close server connection
			server.quit()

			print("successfully sent email to: " + get_email)

			# Save the code into database
			sql_query("UPDATE users SET validation_code = ? WHERE email = ?", [random_word, get_email])

			# Commiting changes
			get_db().commit()

			# Redirect the user
			return redirect("/validate_code")

		flash("This email address is invalid.")
		return redirect("/forgot")

	# GET
	return render_template("forgot.html")


@app.route("/validate_code", methods=["POST", "GET"])
def validate_code():

	# POST
	if request.method == "POST":

		# Get the code from form
		get_code = request.form.get("code")

		# Double check
		if len(get_code) < 10 or get_code == "null" or get_code == "accepted":
			flash("Invalid code.")
			return redirect("/validate_code")

		# Get user_id
		get_uid = sql_query("SELECT id FROM users WHERE validation_code = ?", [get_code])

		# Check if the validation codes match
		if len(get_uid) == 1:

			# Accept the code and update the database
			sql_query("UPDATE users SET validation_code = ? WHERE id = ?", ("accepted", get_uid[0]["id"]))

			# Commiting changes
			get_db().commit()

			return render_template("change_passwd.html", id=get_uid)

		else:

			flash("Invalid code.")
			return redirect("/validate_code")

	# Render the page
	return render_template("validate_code.html")

@app.route("/change_passwd", methods=["POST"])
def change_passwd():

	# Get new password, confirm and id
	new_passwd = request.form.get("password")
	passwd_confirm = request.form.get("confirm")
	uid = request.form.get("uid")

	# Ensure the passwords match
	if not new_passwd or not passwd_confirm:
		flash("Password could not be empty.")
		return render_template("change_passwd.html", id=uid)
	elif new_passwd != passwd_confirm:
		flash("Passwords doesn't match.")
		return render_template("change_passwd.html", id=uid)

	# Get validation_code from database
	query_vcode = sql_query("SELECT validation_code FROM users WHERE id = ?", [uid])

	if query_vcode[0]['validation_code'] == "accepted":

		# Update user password
		hashpw = generate_password_hash(new_passwd)
		sql_query("UPDATE users SET hash_password = ? WHERE id = ?", [hashpw, uid])
		sql_query("UPDATE users SET validation_code = ? WHERE id = ?", ["null", uid])

		# Save password change datetime
		date_now = datetime.datetime.now()
		date = f'{date_now.year}-{date_now.month}-{date_now.day} {date_now.hour}:{date_now.minute}:{date_now.second}'
		sql_query("UPDATE users SET last_pw_change = ? WHERE id = ?", [date, uid])

		# Commiting changes
		get_db().commit()

		flash("The new password has been set.")
		return redirect("/")
	else:

		flash("Invalid code.")
		return redirect("/validate_code")

@app.route("/lectures")
def lectures():

	# Query for lectures
	lectures = sql_query("SELECT lectures.id, title AS l_title, timedate, \
		professors.name AS professor, status, pdf_file_path FROM lectures \
		JOIN professors ON professors.id = lectures.professor")
	
	# GET
	return render_template("lectures.html", lectures=lectures)

@app.route("/new_lecture", methods=["GET", "POST"])
@login_required
def new_lecture():

	# POST
	if request.method == "POST":
		subject = request.form.get("title")
		professor = request.form.get("professor")
		timedate = request.form.get("timedate")
		status = int(request.form.get("status"))
		file = request.files['file']

		# Ensure all fields are filled
		if not subject or not professor or not timedate:
			flash("You need to fill in all fields.")
			return redirect("/new_lecture")
		else:
			# Checks for status field
			if status > 1 or status < 0 or not isinstance(status, int):
				flash("Invalid status")
				return redirect("/new_lecture")

			# Checks for professor and subject fields
			if not isinstance(subject, str) or not isinstance(professor, str):
				flash("Invalid status")
				return redirect("/new_lecture")

			# Checks for the uploaded file
			if file and not allowed_file(file.filename):
				flash("This file extension is not allowed.")
				return redirect("/new_lecture")
			elif file and allowed_file(file.filename):

				# Checks for the lecture's id
				query = sql_query("SELECT MAX(id) AS id FROM lectures")
				try:
					current_id = int(query[0]['id'])
				except:
					current_id = 0

				# Defines next lecture's id
				next_id = current_id + 1

				# Define the filename
				filename = str(next_id) + ".pdf"

				# Save the file
				file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

			if file:
				pdf_file = app.config['UPLOAD_FOLDER'] + "/" + filename
			else:
				pdf_file = "-"

			# Insert into database
			sql_query("INSERT INTO lectures (status, title, timedate, professor, pdf_file_path) VALUES(?, ?, ?, ?, ?)", [status, subject, timedate, professor, pdf_file])
		
			# Commiting changes
			get_db().commit()

			# Redirect the user
			flash("Success!")
			return redirect("/lectures")

	# GET

	professors = sql_query("SELECT * FROM professors ORDER BY name;")
	return render_template("new_lecture.html", professors=professors)

@app.route("/professors", methods=["GET", "POST"])
@login_required
def professors():

	# Query for all professors
	query = sql_query("SELECT id,name FROM professors")

	# POST
	if request.method == "POST":
		professor_name = request.form.get("professor_name")

		if not professor_name:
			flash("Professor's name is empty.")
			return redirect("/professors")
		else:

			# Save into database
			sql_query("INSERT INTO professors (name) VALUES(?)", [professor_name])

			# Commiting changes
			get_db().commit()

			flash("Done.")
			return redirect("/professors")

	# GET
	return render_template("professors.html", professors=query)

@app.route("/delete_professor", methods=["POST"])
@login_required
def delete_professor():

	# Get professor's ID
	p_id = request.form.get("id")

	# Delete
	sql_query("DELETE FROM professors WHERE id = ?", [p_id])

	# Commiting changes
	get_db().commit()

	# Redirect the user
	flash("Deleted.")
	return redirect("/professors")

@app.route("/delete_lecture", methods=["GET", "POST"])
@login_required
def delete_lecture():

	# Query for all Lectures
	lectures = sql_query("SELECT lectures.id AS id, lectures.title AS l_title, professors.name AS professor FROM lectures JOIN professors ON professors.id = lectures.professor ORDER BY lectures.title")

	# POST
	if request.method == "POST":

		# Get information from form
		l_id = request.form.get("id")

		if not l_id:
			flash("You need to choose a lecture to delete!")
			return redirect("/delete_lecture")

		# First, Check if there are enrollments into this lecture
		enrollments = sql_query("SELECT lecture_id FROM enrollments WHERE lecture_id = ?", [l_id])

		if enrollments:
                    get_db().close()
                    flash("Unable to delete this lecture because there are some enrollments into this lecture.")
                    return redirect("/delete_lecture")

		# Delete the lecture
		sql_query("DELETE FROM lectures WHERE id = ?", [l_id])

		# Commiting changes
		get_db().commit()
		
		# Redirect the user
		flash("Done.")
		return redirect("/lectures")

	# GET
	# If admin wants to edit an user into list users page
	try:
		l_id = int(request.args.get("id"))
		if l_id:
			return render_template("delete_lecture.html", lectures=lectures, id=l_id)
	except:
		return render_template("delete_lecture.html", lectures=lectures)

@app.route("/edit_lecture", methods=["GET", "POST"])
@login_required
def edit_lecture():

	# POST
	if request.method == "POST":

		l_id = request.form.get("id")
		subject = request.form.get("title")
		professor = request.form.get("professor")
		timedate = request.form.get("timedate")
		status = int(request.form.get("status"))
		file = request.files['file']

		# Ensure all fields are filled
		if not subject or not professor or not timedate:
			flash("You need to fill in all fields.")
			return redirect("/new_lecture")
		else:
			# Checks for status field
			if status > 1 or status < 0 or not isinstance(status, int):
				flash("Invalid status")
				return redirect("/new_lecture")

			# Checks for professor and subject fields
			if not isinstance(subject, str) or not isinstance(professor, str):
				flash("Invalid status")
				return redirect("/new_lecture")

			# Checks for the uploaded file
			if file and not allowed_file(file.filename):
				flash("This file extension is not allowed.")
				return redirect("/new_lecture")
			elif file and allowed_file(file.filename):

				# Define the filename
				filename = str(l_id) + ".pdf"

				# Save the file
				file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

			# If a new file has been uploaded
			if file:
				pdf_file = app.config['UPLOAD_FOLDER'] + "/" + filename
				sql_query("UPDATE lectures SET status = ?, title = ?, timedate = ?, professor = ?, pdf_file_path = ? WHERE id = ?", [status, subject, timedate, professor, pdf_file, l_id])
			else:
				sql_query("UPDATE lectures SET status = ?, title = ?, timedate = ?, professor = ? WHERE id = ?", [status, subject, timedate, professor, l_id])

			# Commiting changes
			get_db().commit()

			# Redirect the user
			flash("Success!")
			return redirect("/lectures")

	# GET
	l_id = request.args.get("id")

	if l_id:

		# Query for all Lectures
		lecture = sql_query("SELECT lectures.id AS id, lectures.title AS l_title, lectures.status AS status, lectures.timedate AS timedate, lectures.professor AS professor_id, professors.name AS professor_name \
			FROM lectures JOIN professors ON professors.id = lectures.professor WHERE lectures.id = ?", [l_id])

		# Query for all professors
		professors = sql_query("SELECT * FROM professors ORDER BY name")

		status = int(lecture[0]['status'])
		professor_id = lecture[0]['professor_id']
		professor_name = lecture[0]['professor_name']

		return render_template("edit_lecture.html", professors=professors, status=status, professor_id=professor_id, professor_name=professor_name, lecture=lecture)

	else:
		return redirect("/lectures")

@app.route("/remove_pdf_file", methods=["POST"])
@login_required
def remove_pdf_file():

	# Get lecture's id
	l_id = request.form.get("id")

	# Update database
	sql_query("UPDATE lectures SET pdf_file_path = '-' WHERE id= ?", [l_id])

	# Commiting changes
	get_db().commit()

	# Set filename
	filename = l_id + ".pdf"

	# Remove the file
	os.remove(os.path.join(app.config['UPLOAD_FOLDER'], filename))

	# Redirect
	flash("The PDF file has been removed.")
	return redirect("/lectures")

@app.route("/participant_section", methods=["GET"])
def participant_form():

	# Query for countries
	countries = sql_query("SELECT * FROM countries")

	# Query for states
	states = sql_query("SELECT * FROM states")

	return render_template("participant_section.html", countries=countries, states=states)

@app.route("/new_participant", methods=["POST"])
def new_participant():

	# Get values
	f_name = request.form.get("f_name")
	l_name = request.form.get("l_name")
	birthday = request.form.get("birthday")
	email = request.form.get("email")
	country = request.form.get("country")
	state = request.form.get("state")
	city = request.form.get("city")
	phonenumber = request.form.get("phonenumber")
	gender = request.form.get("gender")
	occupation = request.form.get("occupation")

	# Ensure all fiels are filled
	if not f_name or not l_name or not birthday or not email or not country or not state or not city or not phonenumber or not gender:
		flash("You need to fill in all fiels that are marked with (*)")
		return redirect("/participant_section")
	else:

		# Generate a random Registration code
		reg_code = mixedcase_word()

		# Ensure the reg_code is uniq
		row = sql_query("SELECT id FROM persons WHERE registration_code = ?", [reg_code])

		if len(row) > 0:
			reg_code = mixedcase_word()

		# Ensure the email is uniq
		query_for_email = sql_query("SELECT email FROM persons WHERE email = ?", [email])

		try:
			if query_for_email[0]['email']:
				flash("There is another person registered with this email.")
				return redirect("/participant_section")
		except:
			# Insert into database
			sql_query("INSERT INTO persons(first_name, last_name, birthday, email, phonenumber, country, state, city, occupation, registration_code) \
				VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [f_name, l_name, birthday, email, phonenumber, country, state, city, occupation, reg_code])

			# Commiting changes
			get_db().commit()

		# Send the registration code to the participant's email
		msg_reg_code = MIMEMultipart()

		# Configure the message
		message = "Hello, thanks for your registration! Your registration code is: " + reg_code
		msg_reg_code['From'] = SMTP_ADDRESS
		msg_reg_code['To'] = email
		msg_reg_code['Subject'] = "[OneTwoThreeLectures] - Here is your registration code"

		msg_reg_code.attach(MIMEText(message, 'plain'))

		# SMTP Log in
		try:
			server.starttls()
			server.login(msg_reg_code['From'], SMTP_PASSWD)
		except:
			flash("Sorry, SMTP Server is unavailable. Try again later")
			return redirect("/participant_section")

		# Send the email
		server.sendmail(msg_reg_code['From'], msg_reg_code['To'], msg_reg_code.as_string())

		# Close server connection
		server.quit()

		# Log
		print("successfully sent the registration code to: " + email)

		# Redirect the user to main page
		flash("Registered, you will receive an email containing your registration code.")
		return redirect("/")

@app.route("/my_space")
@reg_code_required
def my_space():

	# Get session user_id
	uid = session["person_id"]

	# Query for this username
	username = sql_query("SELECT first_name FROM persons WHERE id = ?", [uid])

	# Query for all lectures that the user is registered for
	lectures = sql_query("SELECT lectures.id AS id, lectures.status AS status, lectures.title AS l_title, lectures.timedate AS timedate, professors.name AS professor_name FROM lectures \
	JOIN professors ON professors.id = lectures.professor \
	JOIN enrollments ON enrollments.lecture_id = lectures.id \
	WHERE enrollments.person_id = ?", [uid])

	return render_template("my_space.html", username = username, lectures=lectures)

@app.route("/check_registration_code", methods=["POST"])
def check_registration_code():

	# Get reg_code
	reg_code = request.form.get("registration_code")

	if not reg_code:
		flash("Registration code's field is empty.")
		return redirect("/participant_section")

	# Ensure the registration code is valid
	query = sql_query("SELECT id FROM persons WHERE registration_code = ?", [reg_code])

	if len(query) == 0:

		flash("Registration code is invalid.")
		return redirect("/participant_section")

	# Save into session
	get_person_id = sql_query("SELECT id FROM persons WHERE registration_code = ?", [reg_code])
	session["person_id"] = get_person_id[0]["id"]

	flash("Welcome to your space.")
	return redirect("/my_space")


@app.route("/quit")
@reg_code_required
def quit():

	# Forget any user_id
	session.pop('person_id', default=None)

	flash('See you soon!')
	return redirect("/")

@app.route("/enroll_lecture", methods=["POST"])
@reg_code_required
def enroll_lecture():

	# Get lecture's ID
	l_id = request.form.get("id")

	# Check wether the user is already registered for this lecture
	query = sql_query("SELECT * FROM enrollments WHERE lecture_id = ? AND person_id = ?", [l_id, session["person_id"]])

	# Ensure that this lecture is open
	if_open = sql_query("SELECT status FROM lectures WHERE id = ?", [l_id])

	if len(query) > 0:

		flash("You are already registered for this lecture.")
		return redirect("/lectures")

	if len(if_open) > 0:

		if int(if_open[0]['status']) == 1:

			print("Already registered into this lecture")
			flash("Sorry, this lecture is already closed.")
			return redirect("/lectures")

	# Insert into database
	try:
		sql_query("INSERT INTO enrollments(lecture_id, person_id) VALUES(?, ?)", [l_id, session["person_id"]])
		# Commiting changes
		get_db().commit()
	except:
		flash("Error while trying to update database.")
		return redirect("/participant_section")

	# Redirect
	flash("Success!")
	return redirect("/my_space")

@app.route("/quit_lecture", methods=["POST"])
@reg_code_required
def quit_lecture():

	# Get values
	l_id = request.form.get("id")

	if l_id:

		# Delete register from database
		sql_query("DELETE FROM enrollments WHERE lecture_id = ? AND person_id = ?", [l_id, session["person_id"]])

		# Commiting changes
		get_db().commit()

		# Redirect the user
		flash("Done.")
		return redirect("/my_space")

	flash("Error trying to delete register from database.")
	return redirect("/my_space")

@app.route("/view_participants")
@login_required
def view_participants():

	# Get Lecture's ID
	l_id = request.args.get("id")
	l_title = sql_query("SELECT title FROM lectures WHERE id = ?", [l_id])

	# Query for the participants of this lecture
	query = sql_query("SELECT persons.first_name AS fname, persons.last_name AS lname FROM persons \
		JOIN enrollments ON enrollments.person_id = persons.id WHERE enrollments.lecture_id = ?", [l_id])

	# Render the template
	return render_template("view_participants.html", participants=query, lecture=l_title, l_id=l_id)

@app.route("/remove_participants", methods=["GET", "POST"])
@login_required
def delete_participants():

	if request.method == "POST":

		# Get Lecture's ID
		l_id = request.form.get("l_id")
		logger.info(l_id)

		# Remove the participants of this lecture
		sql_query("DELETE FROM enrollments WHERE lecture_id = ?", [l_id])

		# Commit
		get_db().commit()

		# Render the template
		flash("Done!")
		return redirect("/lectures")
	
@app.route("/request_certificate", methods=["POST"])
@reg_code_required
def request_certificate():

	# Lecture's ID
	l_id = request.form.get("id")

	# Get Person ID
	person_id = session["person_id"]

	if not person_id:
		flash("First you need to enter with your register code.")
		return redirect("/my_space")
	elif not l_id:
		flash("Error, no lecture's ID was found.")
		return redirect("/my_space")

	# Ensure this is a valid lecture
	query = sql_query("SELECT lectures.id FROM lectures \
		JOIN enrollments ON enrollments.lecture_id = lectures.id \
		WHERE lectures.id = ? AND lectures.status = 1 AND enrollments.person_id = ?", [l_id, person_id])

	if (('query',)) == 0:
		flash("Sorry, You aren't a participant of this lecture or this lecture still open.")
		return redirect("/my_space")

	# Query for this lecture's name
	l_title = sql_query("SELECT title FROM lectures WHERE id = ?", [l_id])

	# Query for lecture's date
	l_date = sql_query("SELECT timedate AS date FROM lectures WHERE id = ?", [l_id])

	# Convert the Date
	regex_date = re.sub("T.*", "", l_date[0]['date'])

	# Query for the professor of this lecture
	l_professor = sql_query("SELECT professors.name AS professor_name FROM professors \
		JOIN lectures ON lectures.professor = professors.id \
		WHERE lectures.id = ?", [l_id])

	# Query for person name
	p_name = sql_query("SELECT first_name, last_name FROM persons WHERE id = ?", [person_id])


	# Call make_certificate function
	p_certificate = make_certificate(l_title[0]['title'], regex_date, l_professor[0]['professor_name'], p_name[0]['first_name'], p_name[0]['last_name'])

	# Redirect to certificate
	return redirect(p_certificate)
