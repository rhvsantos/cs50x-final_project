#!/bin/sh

# Checking variables

_msg() {

    echo "$1 must be set!"
    exit 1

}

[ -z "$SMTP_PASSWD" -o "$SMTP_PASSWD" == "-" ] && { _msg SMTP_PASSWD;  }
[ -z "$API_KEY" -o "$API_KEY" == "-" ] && { _msg API_KEY;  }
[ -z "$SMTP_ADDR" -o "$SMTP_ADDR" == "-" ] && { _msg SMTP_ADDR;  }
[ -z "$SMTP_SERVER" -o "$SMTP_SERVER" == "-" ] && { _msg SMTP_SERVER;  }
[ -z "$SMTP_PASSWD" -o "$SMTP_PASSWD" == "-" ] && { _msg SMTP_SERVER; }
[ -z "$SMTP_SERVER_PORT" -o "$SMTP_SERVER_PORT" == "-" ] && { _msg SMTP_SERVER_PORT;  }

# Starting app
flask run -h 0.0.0.0 --no-reload
#flask run -h 0.0.0.0 --reload --debugger

[ "$?" -eq 0 ] && { exit 0; } || { exit 1; }
