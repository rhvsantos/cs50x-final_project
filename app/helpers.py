import requests, os, string, random, json
from flask import redirect, session, render_template, flash
from functools import wraps

lowercase_letters = string.ascii_lowercase
uppercase_letters = string.ascii_uppercase
letters = string.ascii_letters

ALLOWED_EXTENSIONS = set(['pdf'])

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get("user_id") is None:
            return redirect("/login")
        return f(*args, **kwargs)
    return decorated_function

def reg_code_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get("person_id") is None:
            return redirect("/participant_section")
        return f(*args, **kwargs)
    return decorated_function

# Got from https://kudadam.com/blog/random-words-python
def mixedcase_word():
    word = ''
    random_word_length = random.randint(10,12)
    while len(word) != random_word_length:
        word += random.choice(letters)
    return word

def check_code(email_code, code=200):
    print(email_code)
    return render_template("index.html"), code

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

def make_certificate(l_title, l_date, l_professor, p_fname, p_lname):

    # Using CreftMyPDF API to generate the certificates

    # Define API KEY
    api_key = os.environ.get("API_KEY")

    data = {
        'title': 'Certificate of Archievement',
        'date': l_date,
        'recipient': p_fname + " " + p_lname,
        'desc': 'Has successfully completed the: ' + l_title + ' course.',
        'signature': l_professor
    }

    json_payload = {
        "data": json.dumps(data) ,
        "output_file": "output.pdf",
        "export_type": "json",
        "expiration": 10,
        "template_id": "31a77b2b243d693a"
    }

    response = requests.post(
        F"https://api.craftmypdf.com/v1/create",
        headers = {"X-API-KEY": F"{api_key}"},
        json = json_payload
    )

    json_content = response.json()

    return json_content["file"]




