FROM alpine:latest

LABEL org.opencontainers.image.authors="Rafael Santos"
LABEL description="My app"
LABEL version="1.0"

RUN adduser -g "app" -D -u 1650 app

RUN apk update && apk add py3-flask python3 py3-pip sqlite

USER app
RUN pip install requests flask_session --no-warn-script-location

WORKDIR /app

EXPOSE 5000/tcp

ENV SMTP_PASSWD="-"
ENV API_KEY="-"
ENV SMTP_ADDR="-"
ENV SMTP_SERVER="-"
ENV SMTP_SERVER_PORT="-"
ENV SMTP_PASSWD="-"

CMD ["sh", "-c", "/app/start.sh"]
